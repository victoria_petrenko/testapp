//
//  APIManager.swift
//  TestApp
//
//  Created by Victoria Petrenko on 1/3/19.
//  Copyright © 2019 Victoria Petrenko. All rights reserved.
//

import Foundation

class APIManager {
    
    //MARK: - Properties
    static let sharedInstance = APIManager()
    
    //Session
    fileprivate var session: URLSession!
    fileprivate var configuration : URLSessionConfiguration!
    fileprivate var downloadTask: URLSessionTask?
    
    //MARK: - Public Methods
    func get(url: String, parameters: [String: Any]?, headers: [String: String]? = nil, completion: @escaping ([String:Any]?) -> Void) {
        let fullURl = URL(string: url)!
        fullURl.asyncDownload { data, response, error in
            guard let data = data,
                let dict = (try? JSONSerialization.jsonObject(with: data)) as? [String: Any]
                else {
                    debugPrint("error:", error ?? "nil")
                    return completion(nil)
            }
            completion(dict)
        }
    }
}

extension URL {
    func asyncDownload(completion: @escaping (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> ()) {
        URLSession.shared.dataTask(with: self) {
            completion($0, $1, $2)
            }.resume()
    }
}
