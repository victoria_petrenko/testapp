//
//  PlaceSearchProvider.swift
//  TestApp
//
//  Created by Victoria Petrenko on 1/3/19.
//  Copyright © 2019 Victoria Petrenko. All rights reserved.
//

import Foundation
import CoreData

class PlaceSearchProvider {
    
    //MARK: - Properties
    static let sharedInstance = PlaceSearchProvider()
    var artists: [NSManagedObject] = []
    
    //MARK: - Constants
    enum PlaceSearchProviderConstants {
        static let baseURL = "http://musicbrainz.org/ws/2/artist/?"
        static let limitPageSize = 20
        static let beginYear = "1990"
    }
    
    enum SearchResponseConstants {
        static let placeName = "placeName"
        static let artists = "artists"
        static let artistId = "id"
        static let created = "created"
        static let count = "count"
        static let offset = "offset"
        static let type = "type"
        static let score = "score"
        static let artistName = "name"
        static let sortName = "sort-name"
        static let country = "country"
        static let disambiguation = "disambiguation"
        static let lifeSpan = "life-span"
        static let beginLifeSpan = "begin"
        static let endedLifeSpan = "ended"
    }
    
    //MARK: - Private Methods
    private func parseArtistsResponse(response: [String: Any], placeName: String, pageOffset: Int) -> SearchPlace? {
        
        //Intialize the new SearchPlace entity
        let searchPlace = SearchPlace.new()

        if let savedSearchPlace = SearchPlace.findOne("\(SearchResponseConstants.placeName)=\"\(placeName)\"") {
            //Need delete the old info about place from database
            if let artists = savedSearchPlace.artists {
                searchPlace.addToArtists(artists)
            }
            savedSearchPlace.destroy()
        }
        
        searchPlace.placeName = placeName
        
        if let createdDateText = response[SearchResponseConstants.created] as? String {
            let date = getDate(dateText:createdDateText, format: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", byYear: false)
            searchPlace.created = date
        }
        
        if let count = response[SearchResponseConstants.count] as? Int {
            searchPlace.count = Int32(count)
        }
        
        if let offset = response[SearchResponseConstants.offset] as? Int {
            searchPlace.offset = Int32(offset)
        }
        
        guard let artistsResponse = response[SearchResponseConstants.artists] as? [Any] else {return nil}
        
        for item in artistsResponse {
            if let itemDict = item as? [String: AnyObject] {
                
                let artist = Artist.new()
                
                if let artistId = itemDict[SearchResponseConstants.artistId] as? String, let savedArtist = Artist.findOne("artistId=\"\(artistId)\"")  {
                    //Need delete the old info about artist from database
                    savedArtist.destroy()
                    artist.artistId = artistId
                }
                
                if let type = itemDict[SearchResponseConstants.type] as? String {
                    artist.type = type
                }
                
                if let score = itemDict[SearchResponseConstants.score] as? Int {
                    artist.score = Int32(score)
                }
                
                if let artistName = itemDict[SearchResponseConstants.artistName] as? String {
                    artist.name = artistName
                }
                
                if let sortName = itemDict[SearchResponseConstants.sortName] as? String {
                    artist.sortName = sortName
                }
                
                if let country = itemDict[SearchResponseConstants.country] as? String {
                    artist.country = country
                }
                
                if let disambiguation = itemDict[SearchResponseConstants.disambiguation] as? String {
                    artist.disambiguation = disambiguation
                }
                
                if let lifeSpanDict = itemDict[SearchResponseConstants.lifeSpan] as? [String: Any] {
                    
                    let dateFormat = "yyyy"
                    
                    if let lifeSpanBeginText = lifeSpanDict[SearchResponseConstants.beginLifeSpan] as? String {
                        artist.beginLifeSpan = getDate(dateText: lifeSpanBeginText, format: dateFormat, byYear: true)
                    }
                    
                    if let lifeSpanEndedDate = lifeSpanDict[SearchResponseConstants.endedLifeSpan] as? String {
                        artist.endedLifeSpan = getDate(dateText: lifeSpanEndedDate, format: dateFormat, byYear: true)
                    }
                }
                
                 //Add SearchPlace relationship
                searchPlace.addToArtists(artist)
                //Save Artist entity
                artist.save()
            }
        }
        
        //Save SearchPlace entity
        searchPlace.save()
        return searchPlace
    }
    
    private func getDate(dateText: String, format: String, byYear:Bool) -> Date {
        
        var text = dateText
        if byYear {
            //Need parse only date from full date text
            let yearText = dateText.components(separatedBy: "-")[0]
            text = yearText
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        
        if let date = dateFormatter.date(from: text) {
            return date
        } else {
            return Date()
        }
    }
    
    private func getArtistsArray(searchPlace: SearchPlace) -> [Artist]? {
        guard let artistsArray = searchPlace.artists, let artistsByPlace = artistsArray.allObjects as? [Artist] else { return nil }
        return artistsByPlace
    }
    
    //MARK: - Public Methods
    func loadDBSearchPlaces() -> [SearchPlace] {
        return SearchPlace.findAll(usingSortDescriptors: [NSSortDescriptor(key: SearchResponseConstants.placeName, ascending: true)])
    }
    
    func loadDBArtists(selectedSearchPlace: SearchPlace?) -> [Artist] {
        
        if let searchPlace = selectedSearchPlace, let artistsArray = getArtistsArray(searchPlace: searchPlace) {
            return artistsArray
        } else {
            var artists: [Artist] = []
            let searchPlaces = self.loadDBSearchPlaces()
            for place in searchPlaces {
                if let artistsArray = getArtistsArray(searchPlace: place) {
                    artists.append(contentsOf: artistsArray)
                }
            }
            return artists
        }
    }
    
    func findArtistsByPlace(placeName: String, page: Int, completion: ((SearchPlace?) -> Void)?) {
        
        //Query parameters
        let place = placeName.uppercased()
        let queryText = "query=country:" + place + "%20AND%20" + "begin=" + PlaceSearchProviderConstants.beginYear + "&limit=" + "\(PlaceSearchProviderConstants.limitPageSize)" + "&offset=\(page)"+"&fmt=json"
        
        //Full url
        let url = PlaceSearchProviderConstants.baseURL + queryText
        
        let dispatchQueue = DispatchQueue(label: "QueueIdentification", qos: .background)
        dispatchQueue.async {
            APIManager.sharedInstance.get(url: url, parameters: nil) { (response) in
                
                if let data = response {
                    let searchPlace = self.parseArtistsResponse(response: data, placeName: place, pageOffset: page)
                    
                    let downloadedPage = Int((searchPlace?.offset ?? 0)) + 1
                    var pageSize:Int = 0
                    if let pageCount = searchPlace?.count {
                        pageSize = Int(pageCount)
                    }
                    
                    let currentPageSize = downloadedPage * Int(PlaceSearchProviderConstants.limitPageSize)
                    
                    if currentPageSize < pageSize  {
                        //Start load the next pages
                        self.findArtistsByPlace(placeName: place, page: Int(downloadedPage), completion: completion)
                    }
                    
                    DispatchQueue.main.async {
                        completion?(searchPlace)
                    }
                } else {
                    DispatchQueue.main.async {
                        completion?(nil)
                    }
                }
            }
        }
    }
    
    private func startLoadArtistsByPage(urlText:String, page: Int, searchPlace: SearchPlace, completion: ((SearchPlace?) -> Void)?) {
        
    }
}
