//
//  AlertController+Extension.swift
//  TestApp
//
//  Created by Victoria Petrenko on 1/3/19.
//  Copyright © 2019 Victoria Petrenko. All rights reserved.
//

import Foundation
import UIKit

extension  UIViewController {
    
    func showAlert(title:String, messageText:String, cancelButtonTitle:String, otherButtonTitle:String, completion: ((Bool) -> Swift.Void)? = nil) {
        
        let alert = UIAlertController(title: title,
                                      message: messageText,
                                      preferredStyle: UIAlertController.Style.alert)
        
        if !otherButtonTitle.isEmpty {
            
            let firstButton = UIAlertAction(title:otherButtonTitle,
                                            style:.default,
                                            handler:{(alert: UIAlertAction!) in
                                                
                                                if let completionHander = completion {
                                                    completionHander(false)
                                                }
            })
            
            alert.addAction(firstButton)
        }
        
        if !cancelButtonTitle.isEmpty {
            
            let cencelButton = UIAlertAction(title:cancelButtonTitle,
                                             style:.default,
                                             handler:{(alert: UIAlertAction!) in
                                                
                                                if let completionHander = completion {
                                                    completionHander(true)
                                                }
            })
            alert.addAction(cencelButton)
        }
        
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
}

