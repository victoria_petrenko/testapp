//
//  PlaceSearchTable.swift
//  TestApp
//
//  Created by Victoria Petrenko on 1/3/19.
//  Copyright © 2019 Victoria Petrenko. All rights reserved.
//

import Foundation
import UIKit
import MapKit

protocol PlaceSearchTableDelegate {
    func selectedSearchPlace(searchPlace: SearchPlace?, isSearch: Bool, inViewController: PlaceSearchTable)
}

class PlaceSearchTable : UITableViewController {
    
    //MARK: - Properties
    private var placeSearchProvider = PlaceSearchProvider.sharedInstance
    private var searchPlaces: [SearchPlace] = []
    
    private var searchController:UISearchController?
    
    var delegate:PlaceSearchTableDelegate?
    
    //MARK: - Constants
    enum PlaceSearchTableConstants {
        static let PlaceSearchTable_ID = "PlaceSearchTable"
        static let PlaceSearchTableCell_ID = "PlaceSearchTableCell_ID"
    }
    
    //MARK: - Life-Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: - Private Methods
    private func loadSearchPlaces(placeName: String) {
        searchPlaces = placeSearchProvider.loadDBSearchPlaces()
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
    
    private func dismissController() {
        DispatchQueue.main.async { [weak self] in
            self?.searchController?.dismiss(animated: true)
        }
    }
}

//MARK: - Delegates
//MARK: - UISearchResultsUpdating
extension PlaceSearchTable : UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        
        guard let searchBarText = searchController.searchBar.text else { return }
        searchController.searchBar.delegate = self
        self.searchController = searchController
        loadSearchPlaces(placeName: searchBarText)
    }
}

//MARK: UITableViewDelegate/UITableViewDataSource
extension PlaceSearchTable {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchPlaces.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PlaceSearchTableConstants.PlaceSearchTableCell_ID, for: indexPath)
        cell.textLabel?.text = searchPlaces[indexPath.row].placeName
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedSearchPlace = searchPlaces[indexPath.row]
        self.searchController?.searchBar.text = selectedSearchPlace.placeName
        delegate?.selectedSearchPlace(searchPlace: selectedSearchPlace, isSearch: false, inViewController: self)
        dismissController()
    }
}

//MARK: - UISearchBarDelegate
extension PlaceSearchTable : UISearchBarDelegate {
    
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        guard let searchBarText = searchBar.text else { return }
        placeSearchProvider.findArtistsByPlace(placeName: searchBarText, page: 0) { [weak self] (searchPlace) in
            self?.delegate?.selectedSearchPlace(searchPlace: nil, isSearch: true, inViewController: self!)
            self?.dismissController()
        }
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        delegate?.selectedSearchPlace(searchPlace: nil, isSearch: false, inViewController: self)
        dismissController()
    }
}
