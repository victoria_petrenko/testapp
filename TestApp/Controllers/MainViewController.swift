//
//   MainViewController.swift
//  TestApp
//
//  Created by Victoria Petrenko on 1/3/19.
//  Copyright © 2019 Victoria Petrenko. All rights reserved.
//

import UIKit
import MapKit

class MainViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var mapView: MKMapView!
    
    //MARK: - Properties
    private var locationManager = CLLocationManager()
    private var location: CLLocation?
    private let geocoder = CLGeocoder()
    private var placemark: CLPlacemark?
    private var city:String?
    private var placeSearchController:UISearchController? = nil
    private var placeSearchProvider = PlaceSearchProvider.sharedInstance
    private var artists: [Artist] = []
    
    //MARK: Constants
    enum AlertConstants {
        static let warningText = "Warning"
        static let okText = "Ok"
    }
    
    enum SearchBarConstants {
        static let searchBarPlaceholder = "Search artists by place"
        static let beginYear = 1990
    }
    
    //MARK: - Life-Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureLocationManager()
        initializePlaceSearchController()
    }

    //MARK: - Private methods
    private func configureLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startMonitoringSignificantLocationChanges()
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
    }
    
    private func initializePlaceSearchController() {
        let placeSearchTable = storyboard!.instantiateViewController(withIdentifier:PlaceSearchTable.className) as! PlaceSearchTable
        placeSearchTable.delegate = self
        placeSearchController = UISearchController(searchResultsController: placeSearchTable)
        placeSearchController?.searchResultsUpdater = placeSearchTable
        let searchBar = placeSearchController!.searchBar
        searchBar.sizeToFit()
        searchBar.placeholder = SearchBarConstants.searchBarPlaceholder
        navigationItem.titleView = placeSearchController?.searchBar
        placeSearchController?.hidesNavigationBarDuringPresentation = false
        placeSearchController?.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
    }
    
    private func loadArtists(selectedPlace: SearchPlace?, isSearch: Bool) {
        artists = placeSearchProvider.loadDBArtists(selectedSearchPlace: selectedPlace)
        DispatchQueue.main.async {[weak self] in
            self?.updateMarkersOnMap(isSearch: isSearch)
        }
    }
    
    private func updateCurrentLocation(location: CLLocation?) {
        
        self.location = location
        locationManager.stopUpdatingLocation()
        
        if let currentLocation = location {
            geocoder.reverseGeocodeLocation(currentLocation, completionHandler: { (placemarks, error) in
                
                if error == nil, let placemark = placemarks, !placemark.isEmpty {
                    self.placemark = placemark.last
                }
                
                self.parsePlacemark()
            })
            
            zoomToRegion(location: currentLocation)
        }
    }
    
    private func zoomToRegion(location: CLLocation) {
        
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: location.coordinate, span: span)
        mapView.setRegion(region, animated: true)
    }
    
    private func parsePlacemark() {
        
        guard let placemark = placemark, let city = placemark.locality else {
            debugPrint("Connot to parse placemark")
            return
        }
        
        self.city = city
    }
    
    private func updateMarkersOnMap(isSearch: Bool) {
        
        mapView.removeAnnotations(mapView.annotations)
        
        var annotations: [ArtistAnnotation] = []
        
        let downloadGroup = DispatchGroup()
        
        for artist in self.artists {
            
            let artistAnnotation = ArtistAnnotation()
            artistAnnotation.title = artist.name
            artistAnnotation.subtitle = artist.country
            artistAnnotation.artist = artist
            artistAnnotation.searchPlace = artist.place
            
            if let countryName = artist.country {
                //get location for country
                downloadGroup.enter()
                getLocationCoordinate(countryName: countryName) {(location) in
                    if let artistLocation = location {
                        artistAnnotation.coordinate = self.getApproximateCoordinate(location: artistLocation)
                    }
                    annotations.append(artistAnnotation)
                    downloadGroup.leave()
                }
            }
            
            /*
            let artistAnnotation = MKPointAnnotation()
            artistAnnotation.title = artist.name
            artistAnnotation.subtitle = artist.country
            if let countryName = artist.country {
                //get location for country
                downloadGroup.enter()
                getLocationCoordinate(countryName: countryName) {(location) in
                    if let artistLocation = location {
                        artistAnnotation.coordinate = self.getApproximateCoordinate(location: artistLocation)
                    }
                    annotations.append(artistAnnotation)
                    downloadGroup.leave()
                }
            }
 */
        }
        
        downloadGroup.notify(queue: DispatchQueue.main) { [weak self] in
            self?.mapView.addAnnotations(annotations)
            
            if isSearch {
                //Checck life span date and remove artist if he has ended life span date
                self?.checkLifeSpanDate(annotations: annotations)
            }
            self?.zoomToFitMapAnnotations()
        }
    }
    
    private func getApproximateCoordinate(location:CLLocation)->CLLocationCoordinate2D {
        let x = Float.random(in: 0.0...1.0)
        let randomLat = Float(location.coordinate.latitude) - x
        let randomLongt = Float(location.coordinate.longitude) - x
        return CLLocationCoordinate2D(latitude: CLLocationDegrees(randomLat), longitude: CLLocationDegrees(randomLongt))
    }
    
    private func getLocationCoordinate(countryName: String, completion: ((CLLocation?) -> Void)?) {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(countryName) { (placemarksReponse, errorResponse) in
            if let error = errorResponse {
                debugPrint("Connot find location: error = \(error.localizedDescription)")
                completion?(nil)
            } else if let placemarks = placemarksReponse, placemarks.count > 0, let lastPlaceMark = placemarks.last {
                completion?(lastPlaceMark.location)
            }
        }
    }
    
    private func zoomToFitMapAnnotations() {
        guard mapView.annotations.count > 0 else {
            return
        }
        var topLeftCoord: CLLocationCoordinate2D = CLLocationCoordinate2D()
        topLeftCoord.latitude = -90
        topLeftCoord.longitude = 180
        var bottomRightCoord: CLLocationCoordinate2D = CLLocationCoordinate2D()
        bottomRightCoord.latitude = 90
        bottomRightCoord.longitude = -180
        for annotation: MKAnnotation in mapView.annotations {
            topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude)
            topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude)
            bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude)
            bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude)
        }
        
        var region: MKCoordinateRegion = MKCoordinateRegion()
        region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) * 0.5
        region.center.longitude = topLeftCoord.longitude + (bottomRightCoord.longitude - topLeftCoord.longitude) * 0.5
        region.span.latitudeDelta = fabs(topLeftCoord.latitude - bottomRightCoord.latitude) * 1.36
        region.span.longitudeDelta = fabs(bottomRightCoord.longitude - topLeftCoord.longitude) * 1.36
        
        if region.center.latitude >= -90 && region.center.latitude <= 90 && region.center.longitude >= -180 && region.center.longitude <= 180 && region.span.longitudeDelta >= -360 && region.span.longitudeDelta <= 360 {
            mapView.regionThatFits(region)
            mapView.setRegion(region, animated: true)
        }
    }
    
    private func checkLifeSpanDate(annotations: [ArtistAnnotation]) {
        
        for annotation in annotations {
            if let artist = annotation.artist {
                if let endedLifeSpan = artist.endedLifeSpan {
                    //Check the life-span seconds
                    let endedYear = getYear(date: endedLifeSpan)
                    let beginYear = SearchBarConstants.beginYear
                    let lifeSpanInSeconds = endedYear - beginYear
                    //Need delete annotation from map
                    DispatchQueue.main.async {[weak self] in
                        UIView.animate(withDuration:Double(lifeSpanInSeconds), animations: {
                            self?.mapView.removeAnnotation(annotation)
                        })
                    }
                }
            }
        }
    }
    
    private func getYear(date: Date) -> Int {
        return Calendar.current.component(.year, from: date)
    }
}

//MARK: - Delegates
//MARK: - CLLocationManagerDelegate
extension MainViewController : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if (status == .authorizedWhenInUse) || (status == .authorizedAlways) {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        UIAlertController().showAlert(title: AlertConstants.warningText, messageText: error.localizedDescription, cancelButtonTitle: "", otherButtonTitle: AlertConstants.okText)
        loadArtists(selectedPlace: nil, isSearch: false)
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateToLocation newLocation: CLLocation!, fromLocation oldLocation: CLLocation!) {
        
        updateCurrentLocation(location: newLocation)
        loadArtists(selectedPlace: nil, isSearch: false)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let lastLocation = locations.last {
            updateCurrentLocation(location: lastLocation)
            loadArtists(selectedPlace: nil, isSearch: false)
        }
    }
}

//MARK: - PlaceSearchTableDelegate
extension MainViewController: PlaceSearchTableDelegate {
    func selectedSearchPlace(searchPlace: SearchPlace?, isSearch: Bool, inViewController: PlaceSearchTable) {
        loadArtists(selectedPlace: searchPlace, isSearch: isSearch)
    }
}

//MARK: - MKMapViewDelegate
extension MainViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard annotation is MKPointAnnotation else { return nil }
        
        let identifier = "Annotation"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView!.canShowCallout = true
        } else {
            annotationView!.annotation = annotation
        }
        
        return annotationView
    }
}

//MARK: - Extensions
extension NSObject {
    var className: String {
        return String(describing: type(of: self))
    }
    
    class var className: String {
        return String(describing: self)
    }
}


class ArtistAnnotation : NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    var artist: Artist?
    var searchPlace: SearchPlace?
    
    override init() {
        self.coordinate = CLLocationCoordinate2D()
        self.title = nil
        self.subtitle = nil
    }
}
