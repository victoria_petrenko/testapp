//
//  Artist+CoreDataProperties.swift
//  TestApp
//
//  Created by Victoria Petrenko on 1/3/19.
//  Copyright © 2019 Victoria Petrenko. All rights reserved.

import Foundation
import CoreData


extension Artist {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Artist> {
        return NSFetchRequest<Artist>(entityName: "Artist")
    }

    @NSManaged public var name: String?
    @NSManaged public var artistId: String?
    @NSManaged public var type: String?
    @NSManaged public var score: Int32
    @NSManaged public var sortName: String?
    @NSManaged public var country: String?
    @NSManaged public var disambiguation: String?
    @NSManaged public var beginLifeSpan: Date?
    @NSManaged public var endedLifeSpan: Date?
    @NSManaged public var place: SearchPlace?

}
