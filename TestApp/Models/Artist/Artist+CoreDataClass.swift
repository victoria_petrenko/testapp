//
//  Artist+CoreDataClass.swift
//  TestApp
//
//  Created by Victoria Petrenko on 1/3/19.
//  Copyright © 2019 Victoria Petrenko. All rights reserved.

import Foundation
import CoreData

@objc(Artist)
public class Artist: NSManagedObject, CDHelperEntity {
    public static var entityName: String! { return "Artist" } // Required
}
