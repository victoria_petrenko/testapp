//
//  SearchPlace+CoreDataProperties.swift
//  TestApp
//
//  Created by Victoria Petrenko on 1/3/19.
//  Copyright © 2019 Victoria Petrenko. All rights reserved.

import Foundation
import CoreData


extension SearchPlace {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SearchPlace> {
        return NSFetchRequest<SearchPlace>(entityName: "SearchPlace")
    }

    @NSManaged public var placeName: String?
    @NSManaged public var created: Date?
    @NSManaged public var count: Int32
    @NSManaged public var offset: Int32
    @NSManaged public var artists: NSSet?

}

// MARK: Generated accessors for artists
extension SearchPlace {

    @objc(addArtistsObject:)
    @NSManaged public func addToArtists(_ value: Artist)

    @objc(removeArtistsObject:)
    @NSManaged public func removeFromArtists(_ value: Artist)

    @objc(addArtists:)
    @NSManaged public func addToArtists(_ values: NSSet)

    @objc(removeArtists:)
    @NSManaged public func removeFromArtists(_ values: NSSet)

}
