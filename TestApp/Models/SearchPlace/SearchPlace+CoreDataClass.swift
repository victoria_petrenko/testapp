//
//  SearchPlace+CoreDataClass.swift
//  TestApp
//
//  Created by Victoria Petrenko on 1/3/19.
//  Copyright © 2019 Victoria Petrenko. All rights reserved.

import Foundation
import CoreData

@objc(SearchPlace)
public class SearchPlace: NSManagedObject, CDHelperEntity {
    public static var entityName: String! { return "SearchPlace" } // Required
}
